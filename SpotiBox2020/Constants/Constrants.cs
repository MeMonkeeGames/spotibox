﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpotiBox
{
    public class Constants
    {
        public class Spotify
        {
            public const string StateKey = "spotify_auth_state";
            public const string AccessKeys = "spotify_access_tokens";
        }
    }
}
