﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SpotiBox.Models;

namespace SpotiBox.Controllers
{

    public class SpotifyAuthController : Controller
    {
        private readonly IOptions<Models.Settings.Spotify> spotifyConfig;

        public SpotifyAuthController(IOptions<Models.Settings.Spotify> spotifyConfig)
        {
            this.spotifyConfig = spotifyConfig;
        }

        [Route("SpotifyAuth")]
        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var state = GenerateRandomString(16);

                HttpContext.Session.SetString(Constants.Spotify.StateKey, state);

                Models.Settings.Spotify settings = spotifyConfig.Value;

                Response.Redirect(String.Format("{0}?response_type=code&client_id={1}&scope={2}&redirect_uri={3}&state={4}", "https://accounts.spotify.com/authorize", settings.Client_id, settings.Scope, settings.Redirect_uri, state));
                return Content("redirecting to spotify");
            }
            
            return View();
        }

        [Route("Spotify-callback")]
        public IActionResult Callback()
        {
            if (User.Identity.IsAuthenticated)
            {
                
                var identity = User.Identity as ClaimsIdentity; // Azure AD V2 endpoint specific

                string code = Request.Query["code"];
                string state = Request.Query["state"]; ;
                string storedState = "";

                storedState = HttpContext.Session.GetString(Constants.Spotify.StateKey);

                if (state != null && state == storedState)
                {
                    
  
                    string baseUrl = "https://accounts.spotify.com";

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(baseUrl);
                        using (var request = new HttpRequestMessage(HttpMethod.Post, "/api/token"))
                        {
                            var auth = System.Text.Encoding.UTF8.GetBytes(spotifyConfig.Value.Client_id + ":" + spotifyConfig.Value.Client_secret);
                            request.Headers.Add("Authorization", "Basic " + System.Convert.ToBase64String(auth));
                            
                            var keyValues = new List<KeyValuePair<string, string>>();
                            keyValues.Add(new KeyValuePair<string, string>("code", code));
                            keyValues.Add(new KeyValuePair<string, string>("redirect_uri", spotifyConfig.Value.Redirect_uri));
                            keyValues.Add(new KeyValuePair<string, string>("grant_type", "authorization_code"));
                           // keyValues.Add(new KeyValuePair<string, string>("client_id", spotifyConfig.Value.Client_id));
                            //keyValues.Add(new KeyValuePair<string, string>("client_secret", spotifyConfig.Value.Client_secret));

                            request.Content = new FormUrlEncodedContent(keyValues);
                            using (var response =  client.SendAsync(request).Result)
                            {
                                if (response.IsSuccessStatusCode)
                                {
                                    var data = response.Content.ReadAsStringAsync().Result;
                                    var tokens = JsonConvert.DeserializeObject<SpotifyTokens>(data);
                                    tokens.expires = DateTime.Now.AddSeconds(tokens.expires_in - 60);
                                    tokens.Save(HttpContext);

                                    Response.Redirect("/");
                                }
                            }
                        }                      
                    }
                }

            }

            return Content("");

        }

        [Route("Spotify-refresh")]
        public IActionResult Refresh(string redirectTo)
        {
            if (User.Identity.IsAuthenticated)
            {
                var tokens = SpotifyTokens.Get(HttpContext);
                string baseUrl = "https://accounts.spotify.com";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);
                    using (var request = new HttpRequestMessage(HttpMethod.Post, "/api/token"))
                    {
                        var auth = System.Text.Encoding.UTF8.GetBytes(spotifyConfig.Value.Client_id + ":" + spotifyConfig.Value.Client_secret);
                        request.Headers.Add("Authorization", "Basic " + System.Convert.ToBase64String(auth));

                        var keyValues = new List<KeyValuePair<string, string>>();
                        keyValues.Add(new KeyValuePair<string, string>("refresh_token", tokens.refresh_token));
                        keyValues.Add(new KeyValuePair<string, string>("grant_type", "refresh_token"));

                        request.Content = new FormUrlEncodedContent(keyValues);
                        using (var response = client.SendAsync(request).Result)
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                var data = response.Content.ReadAsStringAsync().Result;
                                var newToken = JsonConvert.DeserializeObject<SpotifyTokens>(data);
                                tokens.access_token = newToken.access_token;
                                tokens.expires = DateTime.Now.AddSeconds(tokens.expires_in - 60);
                                tokens.Save(HttpContext);

                                Response.Redirect(redirectTo);
                            }
                        }
                    }
                }
            }

            return Content("");

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public string GenerateRandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}