﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SpotiBox.Models;
using SpotiBox.Models.Socket;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SpotiBox.Controllers
{
    public class JsonData
    {
        public string Json { get; set; }
    }

    [Route("api/[controller]")]
    public class PlayerController : Controller
    {
        private PlayerHandler _playerHandler { get; set; }

        public PlayerController(PlayerHandler playerHandler)
        {
            _playerHandler = playerHandler;
        }

        [Route("masterUpdate")]
        [HttpPost]
        public JsonResult MasterUpdate([FromBody] JsonData data)
        {
            if (User.Identity.IsAuthenticated)
            {
                CurrentStatus.CurrentStatusJson = data.Json;
                return Json(new { data.Json });
            }

            Response.StatusCode = 403;
            return Json(new { status = "not logged in geeza, leave it out" });
        }

        [Route("currentlyPlaying")]
        public JsonResult CurrentlyPlaying()
        {
            if (User.Identity.IsAuthenticated)
            {
                return Json(new { CurrentStatus.CurrentStatusJson });
            }

            Response.StatusCode = 403;
            return Json(new { status = "not logged in geeza, leave it out" });
        }

    }
}
