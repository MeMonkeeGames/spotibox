﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using SpotiBox.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SpotiBox.Controllers.API
{
    [Route("api/auth")]
    public class AuthController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public JsonResult Get()
        {

            if (User.Identity.IsAuthenticated)
            {
                var identity = User.Identity as ClaimsIdentity; // Azure AD V2 endpoint specific

                string preferred_username = identity.Claims.FirstOrDefault(c => c.Type == "preferred_username")?.Value;

                List<string> claims = new List<string>();
                foreach (var claim in identity.Claims)
                {
                    claims.Add(claim.Type + " : " + claim.Value);
                    
                }

                var spotifyTokens = SpotifyTokens.Get(HttpContext);

                if(spotifyTokens.expires.HasValue && spotifyTokens.expires.Value < DateTime.Now)
                {
                    Response.Redirect("/Spotify-refresh?redirectTo=/api/auth");
                }

                return Json(new { ad = claims, spotifyTokens = spotifyTokens }); 
            }

            return null;
        }
    }
}

