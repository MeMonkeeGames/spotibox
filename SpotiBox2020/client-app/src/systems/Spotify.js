import store from './Store'
import {
    MasterPlayer
} from "@/systems/MasterPlayer"

var player = {};
var interval = "";
var playerTollerance = 2000;
var updateFrequency = 500;

export default {

    init: function () {
        console.log("spotify init");
        window.onSpotifyWebPlaybackSDKReady = function () {};
    }
}.init();

function setupLocalPlayer(options) {

    if (options === undefined) {
        options = {

        }
    }

    window.onSpotifyWebPlaybackSDKReady = () => {
        var tokens = store.getters.spotifyTokens;

        const token = tokens.access_token;

        if (token === undefined || token === '' || token === null) {
            window.location = "/spotifyAuth";
        }

        player = window.spotifyPlayer = new window.Spotify.Player({
            name: 'SpotiBox',
            getOAuthToken: cb => {
                cb(token);
            }
        });

        // Error handling
        //player.addListener('initialization_error', ({
        //    message
        //}) => {

        //    console.error(message);
        //});
        player.addListener('authentication_error', ({
            message
        }) => {
            console.error(message);
            store.dispatch("getAuth");
        });
        //player.addListener('account_error', ({
        //    message
        //}) => {
        //    console.error(message);
        //});
        //player.addListener('playback_error', ({
        //    message
        //}) => {
        //    console.error(message);
        //});

        // Playback status updates
        player.addListener('player_state_changed', state => {

            store.commit('Spotify_State', state);

            if (options.stateChange !== undefined) {
                options.stateChange(state);
            }

            spotifyAPI().user.currentTrack();
            MasterPlayer().update();
        });

        // Ready

        player.addListener('ready', ({
            device_id
        }) => {
            store.commit('LocalPlayerStatus', true);
            spotifyAPI().user.currentTrack();
            spotifyAPI().player.setAsPlayer(device_id);
            interval = setInterval(function () {
                MasterPlayer().update()
            }, updateFrequency);
        });

        // Not Ready
        player.addListener('not_ready', ({
            device_id
        }) => {
            console.log('Device ID has gone offline', device_id);
            store.commit('LocalPlayerStatus', false);

            clearInterval(interval);
        });

        // Connect to the player!
        player.connect();

        spotifyAPI().user.getCurrent();
    };


    if (window.Spotify != undefined && (typeof window.onSpotifyWebPlaybackSDKReady !== 'undefined')) {
        window.onSpotifyWebPlaybackSDKReady();
    }
}

function spotifyAPI() {

    var tokens = store.getters.spotifyTokens;
    var baseURL = "https://api.spotify.com/v1/";
    var fetchOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + tokens.access_token,
        }
    }

    return {

        user: {
            getCurrent: function () {

                fetch(baseURL + 'me', fetchOptions)
                    .then(response => response.json())
                    .then((response) => {
                        store.commit("Spotify_User", response);
                        //console.log(response);
                    })
                    .catch(() => {
                        //console.error(error)
                    });
            },
            currentTrack: function () {

                fetch(baseURL + 'me/player/currently-playing?market=GB', fetchOptions)
                    .then(response => response.json())
                    .then((response) => {
                        store.commit("Spotify_CurrentTrack", response);
                    })
                    .catch(() => {
                        //console.error(error)
                    });
            }
        },
        player: {
            setAsPlayer: function (playerID) {

                var requestData = {
                    device_ids: []
                }

                requestData.device_ids.push(playerID);

                var data = {
                    ...fetchOptions
                };
                data.method = "PUT";
                data.body = JSON.stringify(requestData);

                fetch(baseURL + 'me/player', data)
                    .then(response => response.json())
                    .then((response) => {
                        console.log(response);
                    })
                    .catch(() => {
                        //console.error(error)
                    });
            },
            syncMaster: function (masterState) {

                var requestData = {
                    uris: [masterState.uri],
                    position_ms: masterState.position
                }

                if (!masterState.paused) {

                    if (store.state.localPlay.active && !store.state.localPlay.master) {
                        window.spotifyPlayer.getCurrentState().then(state => {

                            if (state.track_window.current_track.uri != masterState.uri || (state.position < masterState.position - playerTollerance || state.position > masterState.position + playerTollerance)) {

                                console.log("update track");
                                var data = {
                                    ...fetchOptions
                                };
                                data.method = "PUT";
                                data.body = JSON.stringify(requestData);

                                fetch(baseURL + 'me/player/play', data)
                                    .catch((error) => {
                                        console.error(error)
                                    });

                            }
                        })
                    } else {
                        console.log("do nothing");
                    }
                } else {
                    var data = {
                        ...fetchOptions
                    };
                    data.method = "PUT";
                    fetch(baseURL + 'me/player/pause', data)

                        .catch(() => {
                            //console.error(error)
                        });
                }
            }
        }
    }
}

function disablePlayer() {
    window.spotifyPlayer.pause();
    window.spotifyPlayer.disconnect();
    clearInterval(interval);
    store.commit("Spotify_CurrentTrack", undefined);
    store.commit('LocalPlayerStatus', false);
    store.commit('SetMasterPlayer', false);
}

export {
    setupLocalPlayer,
    disablePlayer,
    spotifyAPI
}