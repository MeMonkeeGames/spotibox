﻿import Vue from "vue"

var url = 'wss://' + window.location.host + '/ws'
const socket = new WebSocket(url);

const emitter = new Vue({
  methods: {
    send(message) {
      if (1 === socket.readyState)
        socket.send(message)
    },
    //update(){
    //  if (1 === socket.readyState && store.getters.isMasterPlayer)
    //      socket.send(JSON.stringify(store.state.localPlay))
    //}
  }
})

socket.onmessage = function (msg) {
  emitter.$emit("message", msg.data)
}
socket.onerror = function (err) {
  emitter.$emit("error", err)
}

export default emitter