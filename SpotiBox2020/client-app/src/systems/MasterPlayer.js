import {
    SpotiBoxApi
} from "@/systems/SpotiBoxApi"
import store from './Store'
import Socket from "@/systems/WS"

function MasterPlayer() {

    return {
        update: function () {
            if (store.getters.isMasterPlayer) {
                window.spotifyPlayer.getCurrentState().then(state => {

                    if (state != null) {
                        SpotiBoxApi().master.update(state);

                        var playerState = {
                            uri: state.track_window.current_track.uri,
                            position: state.position,
                            duration: state.track_window.current_track.duration,
                            paused: state.paused
                        }

                        if (store.state.localPlay.master) {
                            var message = {
                                Action: "masterState",
                                Data: JSON.stringify(playerState)
                            }

                            Socket.send(JSON.stringify(message));
                        }
                    }
                });
            }
        }
    }

}

export {
    MasterPlayer
}