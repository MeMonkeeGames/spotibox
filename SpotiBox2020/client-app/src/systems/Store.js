import Vue from 'vue'
import Vuex from 'vuex'
import {Account} from "../Stores/Account"
import {PlayingState} from "../Stores/PlayingState"
import {LocalPlay} from "../Stores/LocalPlay"
import {SpotifyAccount} from "../Stores/SpotifyAccount"
import {WebSocket} from "../Stores/WebSocket"


Vue.use(Vuex)

const store = new Vuex.Store({

  modules: {
    localPlay: LocalPlay,
    account: Account,
    spotifyAccount: SpotifyAccount,
    webSocket: WebSocket,
    playingState: PlayingState
  }
});

export default store