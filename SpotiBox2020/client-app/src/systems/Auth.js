import store from './Store'

export default {

  logout () {
    window.location = "/azureAD/account/logout"
  },

  isLoggedIn () {
    return store.getters.loggedIn;
  },

}
