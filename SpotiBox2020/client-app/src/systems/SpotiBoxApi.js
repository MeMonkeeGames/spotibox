function SpotiBoxApi() {

    var baseURL = "/api/";
    var fetchOptions = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }

    return {
        master: {
            update: function (state) {
                var postData = {
                    Json: JSON.stringify(state)
                }

                var data = {
                    ...fetchOptions
                };
                data.method = "POST";
                data.body = JSON.stringify(postData);

                fetch(baseURL + 'player/masterUpdate', data)
                //.then(response => response.json())            
                //.then((response) => {
                //console.log(JSON.parse(response.json));
                //})
                //.catch((error) => {
                //console.error(error)
                //});

            }
        },

    }
}

export {
    SpotiBoxApi
}