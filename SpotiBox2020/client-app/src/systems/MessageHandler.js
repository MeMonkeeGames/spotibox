﻿import store from './Store'
import {
    spotifyAPI
} from '@/systems/Spotify'

const MessageHandler = function (message) {

    function process(message) {

        switch (message.Action) {
            case 'storeID':
                store.commit('StoreSocketID', message.Data);
                break;

            case 'setAsMaster':
                if (store.state.webSocket.id != message.Sender) {
                    store.commit('SetMasterPlayer', false);
                    console.log(message.Sender + " is now the master");
                } else {
                    console.log("this browser is now the master");
                }
                break;

            case 'masterState':
                var masterState = JSON.parse(message.Data);

                if (!store.state.localPlay.master && store.state.localPlay.active) {
                    spotifyAPI().player.syncMaster(masterState);
                }

                if (store.state.playingState.currentTrack != "updating" && (store.state.playingState.currentTrack === undefined || store.state.playingState.currentTrack.uri != masterState.uri)) {

                    store.dispatch('updateCurrentlyPlaying');
                } else if (store.state.playingState.currentTrack !== undefined) {

                    store.commit('trackPosition', masterState);

                }
                break;

            default:
                //if (message.Data !== null) {
                //    var subMessage = JSON.parse(message.Data);
                //    if (subMessage != undefined) {
                //        process(message);
                //    }
                //}

                console.warn("no message handler set up", message);
                break;
        }
    }

    process(message);

}

export {
    MessageHandler
};