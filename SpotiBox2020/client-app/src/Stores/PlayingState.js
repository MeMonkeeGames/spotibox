import {
    millisToMinutesAndSeconds
} from '../utils/Converters';

const PlayingState = {

    state: {
        currentTrack: undefined,
        position: 0,
        duration: 0,
        paused:false
    },
    mutations: {
        update(state, data) {
            state.currentTrack = data;
            state.duration = data.duration_ms;
            state.paused = data.paused;          
        },
        trackPosition(state, masterstate) {
            state.position = masterstate.position;
            state.paused = masterstate.paused;
            
            if(typeof(state.currentTrack) == "object"){
                state.currentTrack.position = masterstate.position;
            }
        }

    },
    getters: {
        currentTrack: state => {
            return state.currentTrack
        },
        hasCurrentTrack: state => {
            if (state.currentTrack !== undefined && state.currentTrack !== "updating") {
                return true;
            }
            return false;
        },
        currentProgress: state => {
            return millisToMinutesAndSeconds(state.position);
        },
        currentDuration: state => {
            return millisToMinutesAndSeconds(state.duration);
        },
        currentPercentage: state => {
            return ((state.position / state.duration) * 100) + "%";
        },
        isPaused: state => {
            return state.paused;
        }

    },
    actions: {
        updateCurrentlyPlaying({
            commit
        }) {

            commit("update", "updating");

            fetch('/api/player/currentlyPlaying').then(response => response.json())
                .then((response) => {

                    var track = JSON.parse(response.currentStatusJson);

                    if (track.track_window !== undefined) {
                        track = track.track_window.current_track;
                    }
                    commit("update", track);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        }

    }
}

export {
    PlayingState
}