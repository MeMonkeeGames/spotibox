

const Account = {

    state: {
        status: false,
        name: '',
        email: '',
        spotifyTokens: {},
        data: {}
    },
    mutations: {
        AUTH(state, data) {
            state.data = data;

            state.email = data.ad[3].split(":")[1].trim();
            state.name = data.ad[1].split(":")[1].trim();
            state.spotifyTokens = data.spotifyTokens;

            if (state.name != '') {
                state.status = true;
            }
        },
    },
    getters: {
        loggedIn: state => {
            return state.status
        },
        username: state => {
            return state.name
        },
        spotifyTokens: state => {
            return state.spotifyTokens
        }
    },
    actions: {
        getAuth({
            commit
        }) {
            fetch('/api/auth').then(response => response.json())
                .then((response) => {
                    commit("AUTH", response);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        }
    }
}

export {Account}