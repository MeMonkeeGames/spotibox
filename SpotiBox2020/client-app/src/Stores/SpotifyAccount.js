const SpotifyAccount = {

    state: {
        spotifyAccount:{}
    },
    mutations: {
        Spotify_User(state, spotifyAccount){
            state.spotifyAccount = spotifyAccount;
          }
    }
}

export { SpotifyAccount }