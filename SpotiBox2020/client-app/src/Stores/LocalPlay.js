import Socket from "@/systems/WS"

const LocalPlay =  {

    state: {
        active: false,
        spotify: {},
        currentTrack: undefined,
        master: false
    },
    mutations: {
        LocalPlayerStatus(state, status) {
            state.active = status;
        },

        Spotify_State(state, spotifyState) {
            state.spotify = spotifyState;
        },

        Spotify_CurrentTrack(state, currentTrack) {
            state.currentTrack = currentTrack;
        },

        SetMasterPlayer(state, value) {
            if (state.active) {
                state.master = value;
                if (value) {
                    Socket.send('{Action:"setAsMaster"}');
                }
            }
      }
    },
    getters: {
        localPlayActive: state => {
            return state.active;
        },
        spotifyCurrentTrack: state => {
            return state.currentTrack
        },
        spotifyHasTrack: state => {
            if (state.currentTrack !== undefined) {
                return true;
            } else {
                return false;
            }
        },
        isMasterPlayer: state => {
            if (state.active) {
                return state.master;
            } else {
                return false;
            }
        }
    }
}

export {LocalPlay}