﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpotiBox.Models
{
    public class SpotifyTokens
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public DateTime? expires { get; set; }
        public string refresh_token { get; set; }
        public string scope { get; set; }

        public SpotifyTokens()
        {
            
        }

        public SpotifyTokens(string JsonString)
        {
            if (!String.IsNullOrEmpty(JsonString))
            {
                var _data = JsonConvert.DeserializeObject<SpotifyTokens>(JsonString);
                expires = _data.expires;
                access_token = _data.access_token;
                token_type = _data.token_type;
                expires_in = _data.expires_in;
                refresh_token = _data.refresh_token;
                scope = _data.scope;
            }
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this); 
        }


        public void Save(HttpContext _context)
        {
            _context.Session.SetString(Constants.Spotify.AccessKeys, this.ToJson());
        }

        public static SpotifyTokens Get(HttpContext _context)
        {
            return new SpotifyTokens(_context.Session.GetString(Constants.Spotify.AccessKeys));
        }
    }
}
