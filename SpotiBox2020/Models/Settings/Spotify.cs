﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpotiBox.Models.Settings
{
    public class Spotify
    {
        public string Client_id { get; set; }
        public string Client_secret { get; set; }
        public string Redirect_uri { get; set; }
        public string Scope { get; set; }
    }
}
