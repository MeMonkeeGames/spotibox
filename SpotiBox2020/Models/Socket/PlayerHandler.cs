﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;

namespace SpotiBox.Models.Socket
{
    public class PlayerHandler : WebSocketHandler
    {
        public PlayerHandler(ConnectionManager webSocketConnectionManager) : base(webSocketConnectionManager)
        {
        }

        public class Message
        {
            public string Sender { get; set; }
            public string Action { get; set; }
            public string Data { get; set; }
        }

        public override async Task OnConnected(WebSocket socket)
        {
            await base.OnConnected(socket);
            var socketId = ConnectionManager.GetId(socket);
            var message = new Message { Sender = socketId, Action="connected", Data = "is now connected as " };
            await SendMessageToOthersAsync(message);
            await SendMessageAsync(socketId, new Message { Sender = "server", Data = socketId, Action = "storeID"  });
        }

        public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            var socketId = ConnectionManager.GetId(socket);
            var message = new Message{ Sender = socketId, Data = Encoding.UTF8.GetString(buffer, 0, result.Count) };

            try
            {
                var sentMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<Message>(message.Data);
                if (sentMessage != null)
                {
                    sentMessage.Sender = socketId;
                }

                await SendMessageToAllAsync(sentMessage);
            }
            catch
            {
                await SendMessageToAllAsync(message);
            }

        }
    }
}
