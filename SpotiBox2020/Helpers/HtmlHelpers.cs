﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;
using System.Net;


namespace SpotiBox.Helpers
{
    public static class HtmlHelpers
    {

        public static IHtmlContent WebPage(this IHtmlHelper htmlHelper, string url)
        {
            return new HtmlString(new WebClient().DownloadString(url.ToApplicationPath()));
        }
    }
}
